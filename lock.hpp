/*
* Copyright (c) 2012 Robert Nagy
*
* This file is part of CPPExtras.
*
* CPPExtras is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CPPExtras is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CPPExtras. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy (ronag89@gmail.com)
*/
#pragma once

namespace cppex {

template<typename T, typename F>
auto lock(T& mutex, F&& func) -> decltype(func())
{
	struct scoped_lock
	{
		T& mutex;
		
		scoped_lock(T& mutex)
			: mutex(mutex)
		{
			this->mutex.lock();
		}
		
		~scoped_lock()
		{
			this->mutex.unlock();
		}
	} locked(mutex);

	return func();
}

template<typename T, typename F>
auto unlock(T& mutex, F&& func) -> decltype(func())
{
	struct scoped_unlock
	{
		T& mutex;
		
		scoped_unlock(T& mutex)
			: mutex(mutex)
		{
			this->mutex.unlock();
		}
		
		~scoped_unlock()
		{
			this->mutex.lock();
		}
	} unlocked(mutex);

	return func();
}

}