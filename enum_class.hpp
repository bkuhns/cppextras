/*
* Copyright (c) 2012 Robert Nagy
*
* This file is part of CPPExtras.
*
* CPPExtras is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* CPPExtras is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with CPPExtras. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Robert Nagy (ronag89@gmail.com)
*/
#pragma once

namespace cppex {

template<typename def, typename inner = typename def::type>
class enum_class : public def
{
	typedef typename def::type type;
	inner val_; 
public: 
	explicit enum_class(int v) : val_(static_cast<type>(v)) {}
	enum_class(type v) : val_(v) {}
	inner value() const { return val_; }
 
	bool operator==(const enum_class& s) const	{ return val_ == s.val_; }
	bool operator!=(const enum_class& s) const	{ return val_ != s.val_; }
	bool operator<(const enum_class& s) const	{ return val_ <  s.val_; }
	bool operator<=(const enum_class& s) const	{ return val_ <= s.val_; }
	bool operator>(const enum_class& s) const	{ return val_ >  s.val_; }
	bool operator>=(const enum_class& s) const	{ return val_ >= s.val_; }
		
	bool operator==(const int& val) const	{ return val_ == val; }
	bool operator!=(const int& val) const	{ return val_ != val; }
	bool operator<(const int& val) const	{ return val_ <  val; }
	bool operator<=(const int& val) const	{ return val_ <= val; }
	bool operator>(const int& val) const	{ return val_ >  val; }
	bool operator>=(const int& val) const	{ return val_ >= val; }

	enum_class operator&(const enum_class& s) const
	{
		return enum_class(static_cast<type>(val_ & s.val_));
	}

	enum_class& operator&=(const enum_class& s)
	{
		val_ = static_cast<type>(val_ & s.val_);
		return *this;
	}

	enum_class operator|(const enum_class& s) const
	{
		return enum_class(static_cast<type>(val_ | s.val_));
	}
	
	enum_class& operator|=(const enum_class& s)
	{
		val_ = static_cast<type>(val_ | s.val_);
		return *this;
	}	

	enum_class operator^(const enum_class& s) const
	{
		return enum_class(static_cast<type>(val_ ^ s.val_));
	}
	
	enum_class& operator^=(const enum_class& s)
	{
		val_ = static_cast<type>(val_ ^ s.val_);
		return *this;
	}

	enum_class operator~() const
	{
		return enum_class(static_cast<type>(~val_));
	}
};

}